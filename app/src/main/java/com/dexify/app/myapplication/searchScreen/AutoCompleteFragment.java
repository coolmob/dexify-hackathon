package com.dexify.app.myapplication.searchScreen;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.app.LoaderManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.dexify.app.myapplication.Utils.AMLog;
import com.dexify.app.myapplication.Utils.Constants;
import com.dexify.app.myapplication.Utils.Utility;
import com.dexify.app.myapplication.model.AutoCompleteModel;
import com.dexify.app.myapplication.network.ApiUrl;
import com.dexify.app.myapplication.network.ApiUrlConstants;
import com.priya.app.myapplication.R;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AutoCompleteFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AutoCompleteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AutoCompleteFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<AutoCompleteModel>> {

    private AutoCompleteTextView autoCompleteTextView;
    private OnFragmentInteractionListener mListener;
    private List<AutoCompleteModel> completeModelList;
    private ArrayAdapter<AutoCompleteModel> arrayAdapter;

    public AutoCompleteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AutoCompleteFragment.
     */
    public static AutoCompleteFragment newInstance() {
        return new AutoCompleteFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_auto_complete, container, false);

        handleAutoCompleteView(view);
        return view;
    }

    private void handleAutoCompleteView(View view) {
        autoCompleteTextView = (AutoCompleteTextView) view.findViewById(R.id.autocompleteView);
        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.EXTRA_DATA, makeUrl(String.valueOf(s)));
                getLoaderManager().initLoader(0, bundle, AutoCompleteFragment.this).forceLoad();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                AMLog.d(completeModelList.get(position).getText());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onFragmentInteraction(buildIntent(completeModelList.get(position), Constants.ACTION_CITY));
                        getFragmentManager().beginTransaction().remove(AutoCompleteFragment.this).commit();
                        autoCompleteTextView.dismissDropDown();
                        autoCompleteTextView.setAdapter(null);
                        arrayAdapter.notifyDataSetChanged();
                    }
                });

            }
        });
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Loader<List<AutoCompleteModel>> onCreateLoader(int id, Bundle args) {

        String url = args.getString(Constants.EXTRA_DATA);
        return new AutoCompleteLoader(getActivity(), url);
    }

    @Override
    public void onLoadFinished(Loader<List<AutoCompleteModel>> loader, List<AutoCompleteModel> data) {
        if (data != null && !data.isEmpty()) {
            completeModelList = data;
            arrayAdapter = new SearchAdapter(getActivity(), R.layout.item_search_list, data);
            autoCompleteTextView.setAdapter(arrayAdapter);
            autoCompleteTextView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (autoCompleteTextView != null)
                    autoCompleteTextView.showDropDown();
                }
            }, 500);



        }
        getLoaderManager().destroyLoader(0);
    }

    @Override
    public void onLoaderReset(Loader<List<AutoCompleteModel>> loader) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Intent uri);
    }

    /**
     * This class used to download data from the server.
     * we is using this class with AutoCompleteTextView to
     * download and populate data into list.
     */
    private static class AutoCompleteLoader extends AsyncTaskLoader<List<AutoCompleteModel>> {

        ProgressDialog progressDialog;
        String urlString;

        AutoCompleteLoader(Context context, String url) {
            super(context);
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading");
            urlString = url;
        }

        @Override
        public void onCanceled(List<AutoCompleteModel> data) {
            super.onCanceled(data);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            progressDialog.show();
        }

        @Override
        protected void onStopLoading() {
            super.onStopLoading();
        }

        @Override
        protected void onReset() {
            super.onReset();
        }

        @Override
        public List<AutoCompleteModel> loadInBackground() {

            List<AutoCompleteModel> autoCompletes = null;
            try {
                URL url = new URL(urlString);
                String apiResponse = downloadUrl(url);
                autoCompletes = AutoCompleteModel.parseAutoCompleteData(apiResponse);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return autoCompletes;
        }

        @Override
        public void deliverResult(List<AutoCompleteModel> data) {
            super.deliverResult(data);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private Intent buildIntent(AutoCompleteModel selectedObject, String action) {
        Intent intent = new Intent();
        intent.setAction(action);
        intent.putExtra(Constants.EXTRA_VALUE, selectedObject);
        return intent;
    }

    /**
     * Given a URL, sets up a connection and gets the HTTP response body from the server.
     * If the network request is successful, it returns the response body in String form. Otherwise,
     * it will throw an IOException.
     */
    private static String downloadUrl(URL url) throws IOException {
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        String response = null;

        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(3000);
            connection.setReadTimeout(3000);
            // Timeout for reading InputStream arbitrarily set to 3000ms.
            connection.setReadTimeout(3000);
            // Timeout for connection.connect() arbitrarily set to 3000ms.
            connection.setConnectTimeout(3000);
            // For this use case, set HTTP method to GET.
            connection.setRequestMethod("GET");
            // Already true by default but setting just in case; needs to be true since this request
            // is carrying an input (response) body.
            connection.setDoInput(true);
            // Open communications link (network traffic occurs here).
            connection.connect();

            int responseCode = connection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                throw new IOException("HTTP error code: " + responseCode);
            }
            // Retrieve the response body as an InputStream.
            inputStream = connection.getInputStream();
            if (inputStream != null) {
                // Converts Stream to String with max length of 500.
                response = readStream(inputStream, 65000);
            }
        } finally {
            // Close Stream and disconnect HTTPS connection.
            if (inputStream != null) {
                inputStream.close();
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return response;
    }

    /**
     * Converts the contents of an InputStream to a String.
     */
    private static String readStream(InputStream stream, int maxLength) throws IOException {
        String result = null;
        // Read InputStream using the UTF-8 charset.
        InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
        // Create temporary buffer to hold Stream data with specified max length.
        char[] buffer = new char[maxLength];
        // Populate temporary buffer with Stream data.
        int numChars = 0;
        int readSize = 0;
        while (numChars < maxLength && readSize != -1) {
            numChars += readSize;
            int pct = (100 * numChars) / maxLength;
            readSize = reader.read(buffer, numChars, buffer.length - numChars);
        }
        if (numChars != -1) {
            // The stream was not empty.
            // Create String that is actual length of response body if actual length was less than
            // max length.
            numChars = Math.min(numChars, maxLength);
            result = new String(buffer, 0, numChars);
        }
        return result;
    }

    /**
     * making url for auto search
   * @param srchString
   * @return
           */
    private String makeUrl(String srchString) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority(ApiUrl.AUTO_COMPLETE)
                .appendPath("action")
                .appendPath("content")
                .appendPath("zeus")
                .appendPath("autocomplete")
                .appendQueryParameter(ApiUrlConstants.SEARCH_FOR, ApiUrlConstants.VAL_SEARCH_FOR)
                .appendQueryParameter(ApiUrlConstants.NE_CATEGORIES, ApiUrlConstants.VAL_NE_CATEGORIES)
                .appendQueryParameter(ApiUrlConstants.QUERY, srchString);
        return builder.build().toString();
    }
}
