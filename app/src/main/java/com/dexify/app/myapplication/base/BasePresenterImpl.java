package com.dexify.app.myapplication.base;

/**
 * Created by PRIYA on 08-Apr-17.
 */

public interface BasePresenterImpl {
    void bindView(BaseViewPlanner baseViewPlanner);
}
