package com.dexify.app.myapplication.RouteDetailScreen;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;

/**
 * Created by PRIYA on 09-Apr-17.
 */

public class RoutePagerAdapter extends FragmentPagerAdapter {
    private Bundle bundle;

    public RoutePagerAdapter(FragmentManager fm, Bundle bundle) {
        super(fm);
        this.bundle = bundle;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:{
                return RouteFragment.newInstance(bundle);
            }
            default:return null;
        }
    }

    @Override
    public int getCount() {
        return 1;
    }
}
