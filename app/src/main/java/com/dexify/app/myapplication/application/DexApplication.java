package com.dexify.app.myapplication.application;

import android.app.Application;

/**
 * Created by PRIYA on 08-Apr-17.
 */

public class DexApplication extends Application {
    private static DexApplication dexApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        //set the dexApplication
        dexApplication = this;
        //configure the application
    }

    /**
     * Gets the application object
     * @return application object or nil if application is not initialized
     */
    public static DexApplication getApplication() {
        return dexApplication;
    }
}
