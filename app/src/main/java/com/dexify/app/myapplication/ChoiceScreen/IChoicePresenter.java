package com.dexify.app.myapplication.ChoiceScreen;

import com.dexify.app.myapplication.base.BasePresenterImpl;

/**
 * Created by PRIYA on 08-Apr-17.
 */

public interface IChoicePresenter extends BasePresenterImpl{

    void onExploreClick();
    void onRecommendationClick();
}
