package com.dexify.app.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.dexify.app.myapplication.Utils.CustomJsonObject;
import com.dexify.app.myapplication.network.ApiUrlConstants;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by PRIYA on 08-Apr-17.
 */

public class Routes implements Parcelable {
    private String fastestDuration;

    private ArrayList<String> via;

    private String firstModeTypesCss;

    private String allStepModes;

    private ArrayList<String> restModeTypesCss;

    private ArrayList<String> modeTypesCss;

    private ArrayList<Step> steps;

    private String type;

    private String timePretty;

    private String durationPretty;

    private Step firstStep;

    private String time;

    private String durationMinutes;

    private String price;

    private String timePrettySuffix;

    private String modeViaString;

    private String timeUnit;

    private ArrayList<String> layOverTimes;

    private ArrayList<String> modes;

    private String durationHours;

    public String getFastestDuration() {
        return fastestDuration;
    }

    public void setFastestDuration(String fastestDuration) {
        this.fastestDuration = fastestDuration;
    }

    public ArrayList<String> getVia() {
        return via;
    }

    public void setVia(ArrayList<String> via) {
        this.via = via;
    }

    public String getFirstModeTypesCss() {
        return firstModeTypesCss;
    }

    public void setFirstModeTypesCss(String firstModeTypesCss) {
        this.firstModeTypesCss = firstModeTypesCss;
    }

    public String getAllStepModes() {
        return allStepModes;
    }

    public void setAllStepModes(String allStepModes) {
        this.allStepModes = allStepModes;
    }

    public ArrayList<String> getRestModeTypesCss() {
        return restModeTypesCss;
    }

    public void setRestModeTypesCss(ArrayList<String> restModeTypesCss) {
        this.restModeTypesCss = restModeTypesCss;
    }

    public ArrayList<String> getModeTypesCss() {
        return modeTypesCss;
    }

    public void setModeTypesCss(ArrayList<String> modeTypesCss) {
        this.modeTypesCss = modeTypesCss;
    }

    public ArrayList<Step> getSteps() {
        return steps;
    }

    public void setSteps(ArrayList<Step> steps) {
        this.steps = steps;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimePretty() {
        return timePretty;
    }

    public void setTimePretty(String timePretty) {
        this.timePretty = timePretty;
    }

    public String getDurationPretty() {
        return durationPretty;
    }

    public void setDurationPretty(String durationPretty) {
        this.durationPretty = durationPretty;
    }

    public Step getFirstStep() {
        return firstStep;
    }

    public void setFirstStep(Step firstStep) {
        this.firstStep = firstStep;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(String durationMinutes) {
        this.durationMinutes = durationMinutes;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTimePrettySuffix() {
        return timePrettySuffix;
    }

    public void setTimePrettySuffix(String timePrettySuffix) {
        this.timePrettySuffix = timePrettySuffix;
    }

    public String getModeViaString() {
        return modeViaString;
    }

    public void setModeViaString(String modeViaString) {
        this.modeViaString = modeViaString;
    }

    public String getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(String timeUnit) {
        this.timeUnit = timeUnit;
    }

    public ArrayList<String> getLayOverTimes() {
        return layOverTimes;
    }

    public void setLayOverTimes(ArrayList<String> layOverTimes) {
        this.layOverTimes = layOverTimes;
    }

    public ArrayList<String> getModes() {
        return modes;
    }

    public void setModes(ArrayList<String> modes) {
        this.modes = modes;
    }

    public String getDurationHours() {
        return durationHours;
    }

    public void setDurationHours(String durationHours) {
        this.durationHours = durationHours;
    }

    @Override
    public String toString() {
        return "ClassPojo [fastestDuration = " + fastestDuration + ", via = " + via + ", firstModeTypesCss = " + firstModeTypesCss + ", allStepModes = " + allStepModes + ", restModeTypesCss = " + restModeTypesCss + ", modeTypesCss = " + modeTypesCss + ", steps = " + steps + ", type = " + type + ", timePretty = " + timePretty + ", durationPretty = " + durationPretty + ", firstStep = " + firstStep + ", time = " + time + ", durationMinutes = " + durationMinutes + ", price = " + price + ", timePrettySuffix = " + timePrettySuffix + ", modeViaString = " + modeViaString + ", timeUnit = " + timeUnit + ", layOverTimes = " + layOverTimes + ", modes = " + modes + ", durationHours = " + durationHours + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fastestDuration);
        dest.writeStringList(this.via);
        dest.writeString(this.firstModeTypesCss);
        dest.writeString(this.allStepModes);
        dest.writeStringList(this.restModeTypesCss);
        dest.writeStringList(this.modeTypesCss);
        dest.writeTypedList(this.steps);
        dest.writeString(this.type);
        dest.writeString(this.timePretty);
        dest.writeString(this.durationPretty);
        dest.writeParcelable(this.firstStep, flags);
        dest.writeString(this.time);
        dest.writeString(this.durationMinutes);
        dest.writeString(this.price);
        dest.writeString(this.timePrettySuffix);
        dest.writeString(this.modeViaString);
        dest.writeString(this.timeUnit);
        dest.writeStringList(this.layOverTimes);
        dest.writeStringList(this.modes);
        dest.writeString(this.durationHours);
    }

    public Routes() {
    }

    protected Routes(Parcel in) {
        this.fastestDuration = in.readString();
        this.via = in.createStringArrayList();
        this.firstModeTypesCss = in.readString();
        this.allStepModes = in.readString();
        this.restModeTypesCss = in.createStringArrayList();
        this.modeTypesCss = in.createStringArrayList();
        this.steps = in.createTypedArrayList(Step.CREATOR);
        this.type = in.readString();
        this.timePretty = in.readString();
        this.durationPretty = in.readString();
        this.firstStep = in.readParcelable(Step.class.getClassLoader());
        this.time = in.readString();
        this.durationMinutes = in.readString();
        this.price = in.readString();
        this.timePrettySuffix = in.readString();
        this.modeViaString = in.readString();
        this.timeUnit = in.readString();
        this.layOverTimes = in.createStringArrayList();
        this.modes = in.createStringArrayList();
        this.durationHours = in.readString();
    }

    public static final Parcelable.Creator<Routes> CREATOR = new Parcelable.Creator<Routes>() {
        @Override
        public Routes createFromParcel(Parcel source) {
            return new Routes(source);
        }

        @Override
        public Routes[] newArray(int size) {
            return new Routes[size];
        }
    };

    public static Routes parse(CustomJsonObject customJsonObject) throws JSONException {
        Routes routes = new Routes();
        routes.setPrice(customJsonObject.getString(ApiUrlConstants.PRICE));
        routes.setTime(customJsonObject.getString(ApiUrlConstants.TIME));
        routes.setModeViaString(customJsonObject.getString(ApiUrlConstants.MODE_VIA_STRING));
        routes.setTimeUnit(customJsonObject.getString(ApiUrlConstants.TIME_UNIT));
        JSONArray jsonArray = customJsonObject.getJSONArray(ApiUrlConstants.STEPS);
        ArrayList<Step>stepsList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            CustomJsonObject customJsonObject1 = new CustomJsonObject(jsonArray.getString(i));
            stepsList.add(Step.parse(customJsonObject1));
        }
        routes.setSteps(stepsList);
        return routes;
    }
}
