package com.dexify.app.myapplication.searchScreen;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.dexify.app.myapplication.Utils.AMLog;
import com.dexify.app.myapplication.Utils.Constants;
import com.dexify.app.myapplication.model.AutoCompleteModel;
import com.priya.app.myapplication.R;

public class DashBoardActivity extends AppCompatActivity implements AutoCompleteFragment.OnFragmentInteractionListener {

    private String dataAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        extractIntentData();
        bindFragmentView();
    }

    /**
     * binding fragment with activity
     */
    private void bindFragmentView() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        AutoCompleteFragment autoCompleteFragment = AutoCompleteFragment.newInstance();
        fragmentTransaction.replace(R.id.fragment_container, autoCompleteFragment);
        fragmentTransaction.addToBackStack("1");
        fragmentTransaction.commit();
    }

    private void extractIntentData() {
        if(getIntent() != null) {
            dataAction = getIntent().getAction();
        }
    }

    @Override
    public void onFragmentInteraction(Intent intent) {
        if (intent != null) {
            switch (intent.getAction()) {
                case Constants.ACTION_CITY:
                    AutoCompleteModel autoCompleteModel = intent.getParcelableExtra(Constants.EXTRA_VALUE);

                    setResult(RESULT_OK, buildIntent(autoCompleteModel));
                    DashBoardActivity.this.finish();
                    AMLog.d("selected text : " + autoCompleteModel.getText());
                    break;
            }
        }
    }

    /**
     *
     * @param autoCompleteModel
     * @return
     */
    private Intent buildIntent(AutoCompleteModel autoCompleteModel) {
        Intent intent = new Intent();
        intent.setAction(dataAction);
        intent.putExtra(Constants.EXTRA_VALUE, autoCompleteModel);
        return intent;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
