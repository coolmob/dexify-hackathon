package com.dexify.app.myapplication.RouteDetailScreen;

import android.view.View;

/**
 * Created by PRIYA on 09-Apr-17.
 */


public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
