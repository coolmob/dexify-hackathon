package com.dexify.app.myapplication.searchScreen;

/**
 * Created by gaurav lamba on 09/04/17.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dexify.app.myapplication.model.AutoCompleteModel;
import com.priya.app.myapplication.R;

import java.util.List;

/**
 * This adapter is used for setting the list of allowed language in {@link }
 */
public class SearchAdapter extends ArrayAdapter<AutoCompleteModel> {

    private List<AutoCompleteModel> searchResultList;

    public SearchAdapter(Context context, int resourceId, List<AutoCompleteModel> modelList) {
        super(context,resourceId, modelList);
        this.searchResultList = modelList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_search_list, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.searchTextView = (TextView) convertView.findViewById(R.id.searchText);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.searchTextView.setText(searchResultList.get(position).getText());
        convertView.setTag(viewHolder);
        return convertView;
    }

    class ViewHolder {
        TextView searchTextView;
    }
}
