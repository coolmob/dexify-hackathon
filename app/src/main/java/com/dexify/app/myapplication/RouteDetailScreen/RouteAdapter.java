package com.dexify.app.myapplication.RouteDetailScreen;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dexify.app.myapplication.model.Routes;
import com.priya.app.myapplication.R;

import java.util.ArrayList;

/**
 * Created by PRIYA on 09-Apr-17.
 */

public class RouteAdapter extends RecyclerView.Adapter<RouteAdapter.ViewHolder> {
    private ArrayList<Routes>mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtArrTime, txtStops,txtRouteTime;
//        public ImageView icRoute;
        public ViewHolder(View  v) {
            super(v);
            txtArrTime = (TextView) v.findViewById(R.id.txt_route_arr_time);
//            txtDstTime = (TextView) v.findViewById(R.id.txt_route_dst_time);
            txtStops = (TextView) v.findViewById(R.id.txt_route_stops);
            txtRouteTime = (TextView) v.findViewById(R.id.txt_route_time);
//            icRoute = (ImageView) v.findViewById(R.id.img_route_icon);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RouteAdapter(ArrayList<Routes>myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RouteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_route_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Routes routes = mDataset.get(position);
        holder.txtArrTime.setText(routes.getModeViaString());
        holder.txtRouteTime.setText(routes.getTime() + " "+routes.getTimeUnit());
        holder.txtStops.setText(routes.getPrice());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
