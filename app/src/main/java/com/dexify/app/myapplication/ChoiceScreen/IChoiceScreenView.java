package com.dexify.app.myapplication.ChoiceScreen;

import com.dexify.app.myapplication.base.BaseViewPlanner;

/**
 * Created by PRIYA on 08-Apr-17.
 */

public interface IChoiceScreenView extends BaseViewPlanner{

    void wantToExplore();
    void seeRecommendation();
}
