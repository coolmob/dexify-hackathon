package com.dexify.app.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by PRIYA on 08-Apr-17.
 */

public class TfList implements Parcelable {
    private String fare;

    private String classType;

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    @Override
    public String toString() {
        return "ClassPojo [fare = " + fare + ", classType = " + classType + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fare);
        dest.writeString(this.classType);
    }

    public TfList() {
    }

    protected TfList(Parcel in) {
        this.fare = in.readString();
        this.classType = in.readString();
    }

    public static final Parcelable.Creator<TfList> CREATOR = new Parcelable.Creator<TfList>() {
        @Override
        public TfList createFromParcel(Parcel source) {
            return new TfList(source);
        }

        @Override
        public TfList[] newArray(int size) {
            return new TfList[size];
        }
    };
}
