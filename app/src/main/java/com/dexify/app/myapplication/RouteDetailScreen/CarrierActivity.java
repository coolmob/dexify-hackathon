package com.dexify.app.myapplication.RouteDetailScreen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.priya.app.myapplication.R;

public class CarrierActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrier);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        if (null != getIntent()) {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(CarrierFragment.EXTRA_ROUTE_LIST, getIntent().getParcelableArrayListExtra(RouteFragment.EXTRA_CARRIER_LIST));
            getFragmentManager().beginTransaction().replace(R.id.content_carrier, CarrierFragment.newInstance(bundle)).commit();
        }
    }

}
