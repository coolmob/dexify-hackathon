package com.dexify.app.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.dexify.app.myapplication.Utils.CustomJsonObject;
import com.dexify.app.myapplication.network.ApiUrlConstants;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by PRIYA on 08-Apr-17.
 */

public class Step implements Parcelable {
    private String fastestCarrier;

    private String minTime;

    private String originMongoId;

    private String destinationLongitude;

    private String mode;

    private String destinationXid;

    private String distance;

    private String typeInfo;

    private String originLatitude;

    private String destinationMongoId;

    private String originId;

    private String modePretty;

    private String originXid;

    private String originCode;

    private String origin;

    private String fastestCarrierDuration;

//    private null code;

    private String originLongitude;

    private String destinationLatitude;

    private String destination;

    private String destinationCode;

    private ArrayList<Carrier> carriers;

    private String destinationId;

    private String carrierName;

    private String timeUnits;

    private String minPrice;

    public String getFastestCarrier ()
    {
        return fastestCarrier;
    }

    public void setFastestCarrier (String fastestCarrier)
    {
        this.fastestCarrier = fastestCarrier;
    }

    public String getMinTime ()
    {
        return minTime;
    }

    public void setMinTime (String minTime)
    {
        this.minTime = minTime;
    }

    public String getOriginMongoId ()
    {
        return originMongoId;
    }

    public void setOriginMongoId (String originMongoId)
    {
        this.originMongoId = originMongoId;
    }

    public String getDestinationLongitude ()
    {
        return destinationLongitude;
    }

    public void setDestinationLongitude (String destinationLongitude)
    {
        this.destinationLongitude = destinationLongitude;
    }

    public String getMode ()
    {
        return mode;
    }

    public void setMode (String mode)
    {
        this.mode = mode;
    }

    public String getDestinationXid ()
    {
        return destinationXid;
    }

    public void setDestinationXid (String destinationXid)
    {
        this.destinationXid = destinationXid;
    }

    public String getDistance ()
    {
        return distance;
    }

    public void setDistance (String distance)
    {
        this.distance = distance;
    }

    public String getTypeInfo ()
    {
        return typeInfo;
    }

    public void setTypeInfo (String typeInfo)
    {
        this.typeInfo = typeInfo;
    }

    public String getOriginLatitude ()
    {
        return originLatitude;
    }

    public void setOriginLatitude (String originLatitude)
    {
        this.originLatitude = originLatitude;
    }

    public String getDestinationMongoId ()
    {
        return destinationMongoId;
    }

    public void setDestinationMongoId (String destinationMongoId)
    {
        this.destinationMongoId = destinationMongoId;
    }

    public String getOriginId ()
    {
        return originId;
    }

    public void setOriginId (String originId)
    {
        this.originId = originId;
    }

    public String getModePretty ()
    {
        return modePretty;
    }

    public void setModePretty (String modePretty)
    {
        this.modePretty = modePretty;
    }

    public String getOriginXid ()
    {
        return originXid;
    }

    public void setOriginXid (String originXid)
    {
        this.originXid = originXid;
    }

    public String getOriginCode ()
    {
        return originCode;
    }

    public void setOriginCode (String originCode)
    {
        this.originCode = originCode;
    }

    public String getOrigin ()
    {
        return origin;
    }

    public void setOrigin (String origin)
    {
        this.origin = origin;
    }

    public String getFastestCarrierDuration ()
    {
        return fastestCarrierDuration;
    }

    public void setFastestCarrierDuration (String fastestCarrierDuration)
    {
        this.fastestCarrierDuration = fastestCarrierDuration;
    }

//    public null getCode ()
//{
//    return code;
//}
//
//    public void setCode (null code)
//    {
//        this.code = code;
//    }

    public String getOriginLongitude ()
    {
        return originLongitude;
    }

    public void setOriginLongitude (String originLongitude)
    {
        this.originLongitude = originLongitude;
    }

    public String getDestinationLatitude ()
    {
        return destinationLatitude;
    }

    public void setDestinationLatitude (String destinationLatitude)
    {
        this.destinationLatitude = destinationLatitude;
    }

    public String getDestination ()
    {
        return destination;
    }

    public void setDestination (String destination)
    {
        this.destination = destination;
    }

    public String getDestinationCode ()
    {
        return destinationCode;
    }

    public void setDestinationCode (String destinationCode)
    {
        this.destinationCode = destinationCode;
    }

    public ArrayList<Carrier> getCarriers ()
    {
        return carriers;
    }

    public void setCarriers (ArrayList<Carrier> carriers)
    {
        this.carriers = carriers;
    }

    public String getDestinationId ()
    {
        return destinationId;
    }

    public void setDestinationId (String destinationId)
    {
        this.destinationId = destinationId;
    }

    public String getCarrierName ()
    {
        return carrierName;
    }

    public void setCarrierName (String carrierName)
    {
        this.carrierName = carrierName;
    }

    public String getTimeUnits ()
    {
        return timeUnits;
    }

    public void setTimeUnits (String timeUnits)
    {
        this.timeUnits = timeUnits;
    }

    public String getMinPrice ()
    {
        return minPrice;
    }

    public void setMinPrice (String minPrice)
    {
        this.minPrice = minPrice;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [fastestCarrier = "+fastestCarrier+", minTime = "+minTime+", originMongoId = "+originMongoId+", destinationLongitude = "+destinationLongitude+", mode = "+mode+", destinationXid = "+destinationXid+", distance = "+distance+", typeInfo = "+typeInfo+", originLatitude = "+originLatitude+", destinationMongoId = "+destinationMongoId+", originId = "+originId+", modePretty = "+modePretty+", originXid = "+originXid+", originCode = "+originCode+", origin = "+origin+", fastestCarrierDuration = "+fastestCarrierDuration+", code = "+"code"+", originLongitude = "+originLongitude+", destinationLatitude = "+destinationLatitude+", destination = "+destination+", destinationCode = "+destinationCode+", carriers = "+carriers+", destinationId = "+destinationId+", carrierName = "+carrierName+", timeUnits = "+timeUnits+", minPrice = "+minPrice+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fastestCarrier);
        dest.writeString(this.minTime);
        dest.writeString(this.originMongoId);
        dest.writeString(this.destinationLongitude);
        dest.writeString(this.mode);
        dest.writeString(this.destinationXid);
        dest.writeString(this.distance);
        dest.writeString(this.typeInfo);
        dest.writeString(this.originLatitude);
        dest.writeString(this.destinationMongoId);
        dest.writeString(this.originId);
        dest.writeString(this.modePretty);
        dest.writeString(this.originXid);
        dest.writeString(this.originCode);
        dest.writeString(this.origin);
        dest.writeString(this.fastestCarrierDuration);
        dest.writeString(this.originLongitude);
        dest.writeString(this.destinationLatitude);
        dest.writeString(this.destination);
        dest.writeString(this.destinationCode);
        dest.writeTypedList(this.carriers);
        dest.writeString(this.destinationId);
        dest.writeString(this.carrierName);
        dest.writeString(this.timeUnits);
        dest.writeString(this.minPrice);
    }

    public Step() {
    }

    protected Step(Parcel in) {
        this.fastestCarrier = in.readString();
        this.minTime = in.readString();
        this.originMongoId = in.readString();
        this.destinationLongitude = in.readString();
        this.mode = in.readString();
        this.destinationXid = in.readString();
        this.distance = in.readString();
        this.typeInfo = in.readString();
        this.originLatitude = in.readString();
        this.destinationMongoId = in.readString();
        this.originId = in.readString();
        this.modePretty = in.readString();
        this.originXid = in.readString();
        this.originCode = in.readString();
        this.origin = in.readString();
        this.fastestCarrierDuration = in.readString();
        this.originLongitude = in.readString();
        this.destinationLatitude = in.readString();
        this.destination = in.readString();
        this.destinationCode = in.readString();
        this.carriers = in.createTypedArrayList(Carrier.CREATOR);
        this.destinationId = in.readString();
        this.carrierName = in.readString();
        this.timeUnits = in.readString();
        this.minPrice = in.readString();
    }

    public static final Parcelable.Creator<Step> CREATOR = new Parcelable.Creator<Step>() {
        @Override
        public Step createFromParcel(Parcel source) {
            return new Step(source);
        }

        @Override
        public Step[] newArray(int size) {
            return new Step[size];
        }
    };

    /*      "         origin": "Goa",
            "     originCode": "GOI",
            "    destination": "Mumbai",
            "destinationCode": "BOM",
            "           mode": "flight",
            "      originXid": 1072668,
            " destinationXid": 1075798,
            "  originMongoId": "503b2a87e4b032e338f124ab",
            "destinationMongoId": "503b2a90e4b032e338f13ba5",
            "       originId": 1140521,
            "  destinationId": 1140436,
            " fastestCarrier": "Go Air",*/
    public static Step parse(CustomJsonObject customJsonObject){
        Step step = new Step();
        step.setDestinationXid(customJsonObject.getString(ApiUrlConstants.DESTINATION_XID));
        step.setOriginXid(customJsonObject.getString(ApiUrlConstants.ORIGIN_XID));
        step.setDestinationId(customJsonObject.getString(ApiUrlConstants.DESTINATION_ID));
        step.setOriginId(customJsonObject.getString(ApiUrlConstants.ORIGIN_ID));
        step.setMode(customJsonObject.getString(ApiUrlConstants.MODE));
        step.setFastestCarrier(customJsonObject.getString(ApiUrlConstants.FASTEST_CARRIER));
        JSONArray jsonArray = customJsonObject.getJSONArray(ApiUrlConstants.CARRIER);
        ArrayList<Carrier>carrierList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                CustomJsonObject customJsonObject1 = new CustomJsonObject(jsonArray.getString(i));
                Carrier carrier = Carrier.parse(customJsonObject1);
                carrierList.add(carrier);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        step.setCarriers(carrierList);
        return step;
    }
}