package com.dexify.app.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by PRIYA on 08-Apr-17.
 */

public class Flight implements Parcelable {

    private String countryName;

    private String text;

    private String cityId;

    private String cityName;

    private String data;

    private String image;

    private String type;

    private ArrayList<String> destinationCategories;

    private String url;

    private String currency;

    private String price;

    private String name;

    private String stateName;

    public String getCountryName ()
    {
        return countryName;
    }

    public void setCountryName (String countryName)
    {
        this.countryName = countryName;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getCityId ()
    {
        return cityId;
    }

    public void setCityId (String cityId)
    {
        this.cityId = cityId;
    }

    public String getCityName ()
    {
        return cityName;
    }

    public void setCityName (String cityName)
    {
        this.cityName = cityName;
    }

    public String getData ()
    {
        return data;
    }

    public void setData (String data)
    {
        this.data = data;
    }

    public String getImage ()
    {
        return image;
    }

    public void setImage (String image)
    {
        this.image = image;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public ArrayList<String> getDestinationCategories ()
    {
        return destinationCategories;
    }

    public void setDestinationCategories (ArrayList<String> destinationCategories)
    {
        this.destinationCategories = destinationCategories;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getStateName ()
    {
        return stateName;
    }

    public void setStateName (String stateName)
    {
        this.stateName = stateName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [countryName = "+countryName+", text = "+text+", cityId = "+cityId+", cityName = "+cityName+", data = "+data+", image = "+image+", type = "+type+", destinationCategories = "+destinationCategories+", url = "+url+", currency = "+currency+", price = "+price+", name = "+name+", stateName = "+stateName+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.countryName);
        dest.writeString(this.text);
        dest.writeString(this.cityId);
        dest.writeString(this.cityName);
        dest.writeString(this.data);
        dest.writeString(this.image);
        dest.writeString(this.type);
        dest.writeStringList(this.destinationCategories);
        dest.writeString(this.url);
        dest.writeString(this.currency);
        dest.writeString(this.price);
        dest.writeString(this.name);
        dest.writeString(this.stateName);
    }

    public Flight() {
    }

    protected Flight(Parcel in) {
        this.countryName = in.readString();
        this.text = in.readString();
        this.cityId = in.readString();
        this.cityName = in.readString();
        this.data = in.readString();
        this.image = in.readString();
        this.type = in.readString();
        this.destinationCategories = in.createStringArrayList();
        this.url = in.readString();
        this.currency = in.readString();
        this.price = in.readString();
        this.name = in.readString();
        this.stateName = in.readString();
    }

    public static final Parcelable.Creator<Flight> CREATOR = new Parcelable.Creator<Flight>() {
        @Override
        public Flight createFromParcel(Parcel source) {
            return new Flight(source);
        }

        @Override
        public Flight[] newArray(int size) {
            return new Flight[size];
        }
    };
}
