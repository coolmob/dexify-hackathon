package com.dexify.app.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.dexify.app.myapplication.Utils.CustomJsonObject;
import com.dexify.app.myapplication.network.ApiUrlConstants;

import java.util.ArrayList;

/**
 * Created by PRIYA on 08-Apr-17.
 */

public class Carrier implements Parcelable {
    private String depTime;

    private String commaSeparatedDOO;

    private ArrayList<String> busAmenitiesList;

    private String originCode;

    private String code;

    private ArrayList<String> availableClassList;

    private String destinationCode;

    private TfList tfList;

    private String time;

    private String price;

    private String carrierName;

    private String arrTime;

    private String trainType;

    private String daysOfOperation;

    public String getDepTime() {
        return depTime;
    }

    public void setDepTime(String depTime) {
        this.depTime = depTime;
    }

    public String getCommaSeparatedDOO() {
        return commaSeparatedDOO;
    }

    public void setCommaSeparatedDOO(String commaSeparatedDOO) {
        this.commaSeparatedDOO = commaSeparatedDOO;
    }

    public ArrayList<String> getBusAmenitiesList() {
        return busAmenitiesList;
    }

    public void setBusAmenitiesList(ArrayList<String> busAmenitiesList) {
        this.busAmenitiesList = busAmenitiesList;
    }

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArrayList<String> getAvailableClassList() {
        return availableClassList;
    }

    public void setAvailableClassList(ArrayList<String> availableClassList) {
        this.availableClassList = availableClassList;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public TfList getTfList() {
        return tfList;
    }

    public void setTfList(TfList tfList) {
        this.tfList = tfList;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getArrTime() {
        return arrTime;
    }

    public void setArrTime(String arrTime) {
        this.arrTime = arrTime;
    }

    public String getTrainType() {
        return trainType;
    }

    public void setTrainType(String trainType) {
        this.trainType = trainType;
    }

    public String getDaysOfOperation() {
        return daysOfOperation;
    }

    public void setDaysOfOperation(String daysOfOperation) {
        this.daysOfOperation = daysOfOperation;
    }

    @Override
    public String toString() {
        return "ClassPojo [depTime = " + depTime + ", commaSeparatedDOO = " + commaSeparatedDOO + ", busAmenitiesList = " + busAmenitiesList + ", originCode = " + originCode + ", code = " + code + ", availableClassList = " + availableClassList + ", destinationCode = " + destinationCode + ", tfList = " + tfList + ", time = " + time + ", price = " + price + ", carrierName = " + carrierName + ", arrTime = " + arrTime + ", trainType = " + trainType + ", daysOfOperation = " + daysOfOperation + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.depTime);
        dest.writeString(this.commaSeparatedDOO);
        dest.writeStringList(this.busAmenitiesList);
        dest.writeString(this.originCode);
        dest.writeString(this.code);
        dest.writeStringList(this.availableClassList);
        dest.writeString(this.destinationCode);
        dest.writeParcelable(this.tfList, flags);
        dest.writeString(this.time);
        dest.writeString(this.price);
        dest.writeString(this.carrierName);
        dest.writeString(this.arrTime);
        dest.writeString(this.trainType);
        dest.writeString(this.daysOfOperation);
    }

    public Carrier() {
    }

    protected Carrier(Parcel in) {
        this.depTime = in.readString();
        this.commaSeparatedDOO = in.readString();
        this.busAmenitiesList = in.createStringArrayList();
        this.originCode = in.readString();
        this.code = in.readString();
        this.availableClassList = in.createStringArrayList();
        this.destinationCode = in.readString();
        this.tfList = in.readParcelable(TfList.class.getClassLoader());
        this.time = in.readString();
        this.price = in.readString();
        this.carrierName = in.readString();
        this.arrTime = in.readString();
        this.trainType = in.readString();
        this.daysOfOperation = in.readString();
    }

    public static final Parcelable.Creator<Carrier> CREATOR = new Parcelable.Creator<Carrier>() {
        @Override
        public Carrier createFromParcel(Parcel source) {
            return new Carrier(source);
        }

        @Override
        public Carrier[] newArray(int size) {
            return new Carrier[size];
        }
    };

    /*
    "       time": 75,
    "carrierName": "Jet Airways",
    "    depTime": "4:10",
    "    arrTime": "5:25",*/
    public static Carrier parse(CustomJsonObject customJsonObject) {
        Carrier carrier = new Carrier();
        carrier.setTime(customJsonObject.getString(ApiUrlConstants.TIME));
        carrier.setCarrierName(customJsonObject.getString(ApiUrlConstants.CARRIER_NAME));
        carrier.setArrTime(customJsonObject.getString(ApiUrlConstants.ARR_TIME));
        carrier.setDepTime(customJsonObject.getString(ApiUrlConstants.DEP_TIME));
        return carrier;
    }
}


