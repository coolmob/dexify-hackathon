package com.dexify.app.myapplication.ChoiceScreen;

import com.dexify.app.myapplication.base.BaseViewPlanner;

/**
 * Created by PRIYA on 08-Apr-17.
 */

public class ChoicePresenterImpl implements IChoicePresenter {
    IChoiceScreenView iChoiceScreenView;

    @Override
    public void onExploreClick() {
        iChoiceScreenView.wantToExplore();
    }

    @Override
    public void onRecommendationClick() {
        iChoiceScreenView.seeRecommendation();
    }

    @Override
    public void bindView(BaseViewPlanner baseViewPlanner) {
        iChoiceScreenView = (IChoiceScreenView) baseViewPlanner;
    }
}
