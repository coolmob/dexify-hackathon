package com.dexify.app.myapplication.searchScreen;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.dexify.app.myapplication.RouteDetailScreen.RouteDetailActivity;
import com.dexify.app.myapplication.Utils.Constants;
import com.dexify.app.myapplication.model.AutoCompleteModel;
import com.priya.app.myapplication.R;

public class FormActivity extends AppCompatActivity {

    private TextInputEditText departureFromEdt;
    private TextInputEditText destinationToEdt;
    private AutoCompleteModel autoCompleteModelFrom;
    private AutoCompleteModel autoCompleteModelTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        initializeWidgets();
    }

    private void initializeWidgets() {
        handleDestinationFromView();
        handleDestinationToView();
        handleForwardButton();
    }

    private void handleForwardButton() {
        Button button = (Button) findViewById(R.id.forward_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (departureFromEdt.getText().length() > 0 && destinationToEdt.getText().length() > 0) {
                    startRouteActivity();
                } else {
                    Toast.makeText(FormActivity.this, "Please fill details first", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Here initializing widgets and handling events
     */
    private void handleDestinationFromView() {
        departureFromEdt = (TextInputEditText) findViewById(R.id.fromEditView);

        departureFromEdt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        startSearchActivity(Constants.ACTION_FROM);
                        break;
                }
                return true;
            }
        });
    }

    /**
     * Here initializing widgets and handling events
     */
    private void handleDestinationToView() {
        destinationToEdt = (TextInputEditText) findViewById(R.id.toEditView);

        destinationToEdt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        startSearchActivity(Constants.ACTION_TO);
                        break;
                }
                return true;
            }
        });
    }

    /**
     * Attempt to start next activity to download data
     * @param action used to take decision on next activity
     */
    private void startSearchActivity(String action) {
        Intent intent = new Intent(this, DashBoardActivity.class);
        intent.setAction(action);
        startActivityForResult(intent, 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 100) {
                switch (data.getAction()) {
                    case Constants.ACTION_TO:
                        autoCompleteModelTo = data.getParcelableExtra(Constants.EXTRA_VALUE);
                        destinationToEdt.setText(autoCompleteModelTo.getText());
                        break;
                    case Constants.ACTION_FROM:
                        autoCompleteModelFrom = data.getParcelableExtra(Constants.EXTRA_VALUE);
                        departureFromEdt.setText(autoCompleteModelFrom.getText());
                        break;
                }
            }
        }
    }

    private void startRouteActivity() {
        Intent intent = new Intent(this, RouteDetailActivity.class);
        intent.putExtra(Constants.EXTRA_FROM, autoCompleteModelFrom);
        intent.putExtra(Constants.EXTRA_TO, autoCompleteModelTo);
        startActivity(intent);
    }
}
