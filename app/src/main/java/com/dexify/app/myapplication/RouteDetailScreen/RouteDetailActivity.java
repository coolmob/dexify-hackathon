package com.dexify.app.myapplication.RouteDetailScreen;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.android.volley.Request;
import com.dexify.app.myapplication.ApiParams;
import com.dexify.app.myapplication.Utils.AMLog;
import com.dexify.app.myapplication.Utils.Constants;
import com.dexify.app.myapplication.Utils.CustomJsonObject;
import com.dexify.app.myapplication.model.AutoCompleteModel;
import com.dexify.app.myapplication.model.RouteData;
import com.dexify.app.myapplication.model.Routes;
import com.dexify.app.myapplication.network.ApiCall;
import com.dexify.app.myapplication.network.ApiUrl;
import com.dexify.app.myapplication.network.ApiUrlConstants;
import com.dexify.app.myapplication.network.RequestComplete;
import com.dexify.app.myapplication.network.ResponseObject;
import com.priya.app.myapplication.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class RouteDetailActivity extends AppCompatActivity {

    private ViewPager pager;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        initViews();
        extractParcellData();
    }

    private void extractParcellData() {
        if (getIntent() != null) {
            AutoCompleteModel autoCompleteModelFrom = getIntent().getParcelableExtra(Constants.EXTRA_FROM);
            AutoCompleteModel autoCompleteModelTo = getIntent().getParcelableExtra(Constants.EXTRA_TO);
            hitA2BApi(autoCompleteModelFrom.getXid(), autoCompleteModelTo.getXid());
        }
    }

    private void initViews() {
        pager = (ViewPager)findViewById(R.id.pager);
    }

    private void hitA2BApi(String originCityId, String destinationCityId){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        ApiParams apiParams = new ApiParams(Request.Method.GET, ApiUrl.A2B_URL);
        apiParams.addParamPair(ApiUrlConstants.API_KEY, ApiUrlConstants.VAL_API_KEY);
        apiParams.addParamPair(ApiUrlConstants.ORIGIN_CITY_ID, originCityId);
        apiParams.addParamPair(ApiUrlConstants.DESTINATION_CITY_ID,destinationCityId);
        ApiCall.stringGetRequestWithParam(apiParams.createRequestParams("autoComplete2"), new RequestComplete() {
            @Override
            public void requestSuccess(ResponseObject object) {
                AMLog.d(object.toString());
                progressDialog.dismiss();
                try {
                    RouteData routeData = new RouteData();
                    CustomJsonObject rootObj = new CustomJsonObject((String)object.getData());
                    CustomJsonObject dataObject = rootObj.getJSONObject(ApiUrlConstants.DATA);
                    JSONArray routeArr = dataObject.getJSONArray("routes");
                    if(null != routeArr) {
                        ArrayList<Routes>routeList = new ArrayList<Routes>();
                        for (int i = 0; i < routeArr.length(); i++) {
                            CustomJsonObject route = new CustomJsonObject(routeArr.getString(i));
                            routeList.add(Routes.parse(route));
                        }
                        routeData.setRoutes(routeList);
                    }

                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList(CarrierFragment.EXTRA_ROUTE_LIST,routeData.getRoutes());
                    pager.setAdapter(new RoutePagerAdapter(getFragmentManager(),bundle));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void requestFailed(ResponseObject object) {
                progressDialog.dismiss();
                AMLog.d(object.toString());
            }
        });
    }

}
