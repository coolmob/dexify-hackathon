package com.dexify.app.myapplication.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Map;

/**
 * Created by priya on 11/20/15.
 */
public class CustomJsonObject extends JSONObject {

  public static final int PARSE_INT_DEFAULT_VALUE = -1;
  public static final int PARSE_DOUBLE_DEFAULT_VALUE = -1;
  public static final int PARSE_LONG_DEFAULT_VALUE = -1;
  public CustomJsonObject(String jsonResponse) throws JSONException {
    super(jsonResponse);
  }

  public CustomJsonObject(Map copyFrom) {
    super(copyFrom);
  }

  public CustomJsonObject(JSONTokener readFrom) throws JSONException {
    super(readFrom);
  }

  public CustomJsonObject(JSONObject copyFrom, String[] names) throws JSONException {
    super(copyFrom, names);
  }

  public CustomJsonObject(JSONObject jsonObject) throws JSONException {
    this(jsonObject.toString());
  }

  @Override
  public String getString(String name) {
    String value = null;
    try {
      value = super.getString(name);
      if (null != value && value.equalsIgnoreCase("null")) {
        return null;
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return value;
  }

  @Override
  public int getInt(String name) {
    int value = PARSE_INT_DEFAULT_VALUE;
    try {
      String strVal = super.getString(name);
      if (null != strVal && !strVal.isEmpty() && !strVal.equals(NULL)) {
        value = super.getInt(name);
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return value;
  }

  @Override
  public boolean getBoolean(String name) {
    boolean value = false;
    try {
      value = super.getBoolean(name);
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return value;
  }

  @Override
  public double getDouble(String name) {
    double value = PARSE_DOUBLE_DEFAULT_VALUE;
    try {
      value = super.getDouble(name);
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return value;
  }

  @Override
  public JSONArray getJSONArray(String name) {
    JSONArray value = null;
    try {
      value = super.getJSONArray(name);
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return value;
  }

  @Override
  public CustomJsonObject getJSONObject(String name) {
    CustomJsonObject value = null;
    try {
      JSONObject dummy = super.getJSONObject(name);
      if (null != dummy) {
        value = new CustomJsonObject(dummy.toString());
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return value;
  }

  @Override
  public long getLong(String name) {
    long value = PARSE_LONG_DEFAULT_VALUE;
    try {
      value = super.getLong(name);
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return value;
  }


//  public CustomJsonObject getCustomObject(String name) throws JSONException {
//    CustomJsonObject  value = null;
//    value = super.opt(name);
//    return (CustomJsonObject) value;
//  }
}
