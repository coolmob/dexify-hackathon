package com.dexify.app.myapplication.Utils;

/**
 * Created by shalutyagi on 09/04/17.
 */

public class Constants {

    public static final String ACTION_CITY = "action_city";
    public static final String EXTRA_VALUE = "extra_value";
    public static final String ACTION_TO = "action_to";
    public static final String ACTION_FROM = "action_from";
    public static final String EXTRA_DATA = "extra_data";
    public static final String EXTRA_TO = "extra_to";
    public static final String EXTRA_FROM = "extra_from";
}
