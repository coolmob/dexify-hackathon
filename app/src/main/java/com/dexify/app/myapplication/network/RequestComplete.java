package com.dexify.app.myapplication.network;

public interface RequestComplete {
  void requestSuccess(ResponseObject object);
  void requestFailed(ResponseObject object);
}
