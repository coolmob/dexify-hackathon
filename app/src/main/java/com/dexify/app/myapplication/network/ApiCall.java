package com.dexify.app.myapplication.network;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dexify.app.myapplication.Utils.AMLog;
import com.dexify.app.myapplication.network.params.RequestParams;
import com.dexify.app.myapplication.network.request.StringGetParamRequest;
import com.dexify.app.myapplication.network.request.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;

/**
 * Class for web api calls
 */
public class ApiCall {
    public static final String EXTRA_FORCE_UPDATE_INFO = "EXTRA_FORCE_UPDATE_INFO";
    private static final String API_KEY_FORCE_UPDATE = "forceUpdate";
    private static final String API_KEY_DATA = "data";

    /**
     * This method used to make connection and fetch data from a 3rd party network Api such as wordpress with help of {@link Volley}
     * with StringRequest.
     *
     * @param requestParam    request parameter
     * @param requestComplete completion block
     * @return the request object
     */
    public static StringGetParamRequest stringRequestThirdPartyApi(RequestParams requestParam, final RequestComplete requestComplete) {
        AMLog.d("Hitting: " + requestParam.getUrl());
        final StringGetParamRequest stringGetParamRequest = StringGetParamRequest.createRequest(requestParam, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                AMLog.d("Network response:" + response);
                ResponseHandler.handleSuccessResponse(response, requestComplete, ResponseHandler.ApiType.THIRD_PARTY_API);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AMLog.e(error);
                ResponseHandler.handleFailureResponse(error, requestComplete, ResponseHandler.ApiType.THIRD_PARTY_API);
            }
        });

        //set Retry policy
        stringGetParamRequest.setRetryPolicy(ApiCall.getRetryPolicy());
        //set params
        stringGetParamRequest.setRequestParams(requestParam);
        // disable caching by volley
        stringGetParamRequest.setShouldCache(false);

        //execute the request
        stringGetParamRequest.executeRequest();
        return stringGetParamRequest;
    }

    /**
     * This method used to make connection and fetch data from network with help of {@link Volley}
     * with StringRequest.
     *
     * @param requestParam    request parameter
     * @param requestComplete completion block
     * @return the request object
     */
    public static StringRequest stringRequest(RequestParams requestParam, final RequestComplete requestComplete) {
        AMLog.d("Hitting: " + requestParam.getUrl());
        final StringRequest stringRequest = new StringRequest(requestParam, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                AMLog.d("Network response:" + response);
                ResponseHandler.handleSuccessResponse(response, requestComplete, ResponseHandler.ApiType.AM_API);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AMLog.e(error);
                ResponseHandler.handleFailureResponse(error, requestComplete, ResponseHandler.ApiType.AM_API);
            }
        });

        //set Retry policy
        //stringRequest.setRetryPolicy(ApiCall.getRetryPolicy());
        //set params
        stringRequest.setRequestParams(requestParam);
        // disable caching by volley
        stringRequest.setShouldCache(false);

        //execute the request
        stringRequest.executeRequest();
        return stringRequest;
    }

    /**
     * This is the Overloaded method of StringRequest.
     * <p/>
     * This method used to make connection and fetch data from network with help of {@link Volley}
     * with StringRequest.
     * <p/>
     * By using this Overloaded method can set the Timeout time in millisecond to increase the request time
     *
     * @param requestParam    request parameter
     * @param retryPolicy     retry policy
     * @param requestComplete completion block
     * @return the request object
     */
    public static StringRequest stringRequest(RequestParams requestParam, RetryPolicy retryPolicy, final RequestComplete requestComplete) {

        final StringRequest stringRequest = stringRequest(requestParam, requestComplete);

        if (retryPolicy != null) {
            stringRequest.setRetryPolicy(retryPolicy);
        }
// already getting executed once in stringRequest(RequestParams, RequestComplete)
//    //execute the request
//    stringRequest.executeRequest();
        return stringRequest;
    }

    /**
     * Use this method when you have parameters in GET request
     *
     * @param requestParam    Map of key/value required in get request
     * @param requestComplete completion block
     */
    public static StringGetParamRequest stringGetRequestWithParam(RequestParams requestParam, final RequestComplete requestComplete) {
        AMLog.d("Hitting : " + requestParam.getUrl());
        final StringGetParamRequest stringGetParamRequest = StringGetParamRequest.createRequest(requestParam, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                AMLog.d("Network response:" + response);
                ResponseHandler.handleSuccessResponse(response, requestComplete, ResponseHandler.ApiType.THIRD_PARTY_API);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ResponseHandler.handleFailureResponse(error, requestComplete, ResponseHandler.ApiType.THIRD_PARTY_API);
            }
        });


//TODO : need to check the execute request

        //set Retry policy
        stringGetParamRequest.setRetryPolicy(ApiCall.getRetryPolicy());
        //set params
        stringGetParamRequest.setRequestParams(requestParam);
        // disable caching by volley
        stringGetParamRequest.setShouldCache(false);
        //execute the request
        stringGetParamRequest.executeRequest();
        return stringGetParamRequest;
    }

    /**
     * Checks for forceUpdate param in the response from the server, if the param is present and has value 1 than this method returns true else false
     *
     * @param error {@link VolleyError}
     * @return true if forceUpdate has value 1,false otherwise whatever the reason may be
     */
    private static String checkForceUpdate(VolleyError error) {
        if (null != error) {
            NetworkResponse networkResponse = error.networkResponse;
            if (null != networkResponse) {
                byte[] data = networkResponse.data;
                if (null != data) {
                    try {
                        String response = new String(data, HttpHeaderParser.parseCharset(networkResponse.headers));
                        AMLog.d("Force" + response);
                        if (!response.isEmpty()) {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(API_KEY_FORCE_UPDATE)) {
                                if (jsonObject.optInt(API_KEY_FORCE_UPDATE, 0) > 0) {
                                    return jsonObject.optString(API_KEY_DATA, null);
                                }
                            }
                        }
                    } catch (UnsupportedEncodingException | JSONException e) {
                        AMLog.e(e);
                    }
                }
            }
        }
        return null;
    }

    private static DefaultRetryPolicy getRetryPolicy() {
        final int TIMEOUT_SEC = 35;
        final int DEFAULT_MAX_RETRIES = 0;
        final int DEFAULT_TIMEOUT_MS = (int) TimeUnit.SECONDS.toMillis(TIMEOUT_SEC);

        return (new DefaultRetryPolicy(DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
}