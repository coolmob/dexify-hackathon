package com.dexify.app.myapplication.RouteDetailScreen;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dexify.app.myapplication.Utils.AMLog;
import com.dexify.app.myapplication.model.Carrier;
import com.dexify.app.myapplication.model.Routes;
import com.dexify.app.myapplication.model.Step;
import com.priya.app.myapplication.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RouteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RouteFragment extends Fragment {

    public static final String EXTRA_ROUTE_LIST = "EXTRA_ROUTE_LIST";
    public static final String EXTRA_CARRIER_LIST = "EXTRA_CARRIER_LIST";
    private ArrayList<Routes> routesArrayList;
    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;

    public RouteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CarrierFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RouteFragment newInstance(Bundle bundle) {
        RouteFragment fragment = new RouteFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            routesArrayList = getArguments().getParcelableArrayList(EXTRA_ROUTE_LIST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cheap_route, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_cheap_route);
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager 
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(new RouteAdapter(routesArrayList));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),mLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.addOnItemTouchListener(new RecyclerClickListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                AMLog.d("object at position : " + position + " clicked");
                ArrayList<Carrier> carrierArrayList = new ArrayList<Carrier>();
                for (Step step : routesArrayList.get(position).getSteps()) {
                    carrierArrayList.addAll(step.getCarriers());
                }
                Intent intent = new Intent(getActivity(), CarrierActivity.class);
                intent.putParcelableArrayListExtra(EXTRA_CARRIER_LIST, carrierArrayList);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
