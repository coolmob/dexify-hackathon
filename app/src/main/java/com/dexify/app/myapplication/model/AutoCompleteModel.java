package com.dexify.app.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.dexify.app.myapplication.network.ApiUrlConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gaurav lamba on 08/04/17.
 */


/*    {
        text: "new delhi",
        url: "/travel-guide/new-delhi",
        ct: "explore-destination",
        address: "",
        _id: "503b2a70e4b032e338f0ee67",
        cn: null,
        en: true,
        rt: "",
        st: "delhi",
        co: "india",
        _oid: 1140454,
        eid: "503b2a70e4b032e338f0ee67",
        cid: null,
        useNLP: false,
        lat: 28.61282,
        lon: 77.23114,
        xid: 1065223
    }*/
public class AutoCompleteModel implements Parcelable {

    private String text;
    private String url;
    private String ct;
    private String address;
    private String _id;
    private String cn;
    private String en;
    private String rt;
    private String st;
    private String co;
    private String _oid;
    private String eid;
    private String cid;
    private String useNLP;
    private String lat;
    private String lon;
    private String xid;

    public AutoCompleteModel() {}

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCt() {
        return ct;
    }

    public void setCt(String ct) {
        this.ct = ct;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getRt() {
        return rt;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public String get_oid() {
        return _oid;
    }

    public void set_oid(String _oid) {
        this._oid = _oid;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getUseNLP() {
        return useNLP;
    }

    public void setUseNLP(String useNLP) {
        this.useNLP = useNLP;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getXid() {
        return xid;
    }

    public void setXid(String xid) {
        this.xid = xid;
    }

    public static List<AutoCompleteModel> parseAutoCompleteData(String apiResponse) throws JSONException {

        List<AutoCompleteModel> autoCompleteList = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(apiResponse);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
            AutoCompleteModel autoComplete = new AutoCompleteModel();
            autoComplete.setText(jsonObject.getString(ApiUrlConstants.TEXT));
            autoComplete.setUrl(jsonObject.getString(ApiUrlConstants.URL));
            autoComplete.setCt(jsonObject.getString(ApiUrlConstants.CT));
            autoComplete.set_id(jsonObject.getString(ApiUrlConstants._ID));
            autoComplete.setCn(jsonObject.getString(ApiUrlConstants.CN));
            autoComplete.setAddress(jsonObject.getString(ApiUrlConstants.ADDRESS));
            autoComplete.setEn(jsonObject.getString(ApiUrlConstants.EN));
            autoComplete.setRt(jsonObject.getString(ApiUrlConstants.RT));
            autoComplete.setSt(jsonObject.getString(ApiUrlConstants.ST));
            autoComplete.setCo(jsonObject.getString(ApiUrlConstants.CO));
            autoComplete.set_oid(jsonObject.getString(ApiUrlConstants._OID));
            autoComplete.setEid(jsonObject.getString(ApiUrlConstants.EID));
            autoComplete.setCid(jsonObject.getString(ApiUrlConstants.CID));
            autoComplete.setUseNLP(jsonObject.getString(ApiUrlConstants.USE_NLP));
            autoComplete.setLat(jsonObject.getString(ApiUrlConstants.LAT));
            autoComplete.setLon(jsonObject.getString(ApiUrlConstants.LON));
            autoComplete.setXid(jsonObject.getString(ApiUrlConstants.XID));
            autoCompleteList.add(autoComplete);
        }
        return autoCompleteList;
    }

    @Override
    public String toString() {
        return this.getText();
    }

    protected AutoCompleteModel(Parcel in) {
        text = in.readString();
        url = in.readString();
        ct = in.readString();
        address = in.readString();
        _id = in.readString();
        cn = in.readString();
        en = in.readString();
        rt = in.readString();
        st = in.readString();
        co = in.readString();
        _oid = in.readString();
        eid = in.readString();
        cid = in.readString();
        useNLP = in.readString();
        lat = in.readString();
        lon = in.readString();
        xid = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
        dest.writeString(url);
        dest.writeString(ct);
        dest.writeString(address);
        dest.writeString(_id);
        dest.writeString(cn);
        dest.writeString(en);
        dest.writeString(rt);
        dest.writeString(st);
        dest.writeString(co);
        dest.writeString(_oid);
        dest.writeString(eid);
        dest.writeString(cid);
        dest.writeString(useNLP);
        dest.writeString(lat);
        dest.writeString(lon);
        dest.writeString(xid);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AutoCompleteModel> CREATOR = new Parcelable.Creator<AutoCompleteModel>() {
        @Override
        public AutoCompleteModel createFromParcel(Parcel in) {
            return new AutoCompleteModel(in);
        }

        @Override
        public AutoCompleteModel[] newArray(int size) {
            return new AutoCompleteModel[size];
        }
    };
}