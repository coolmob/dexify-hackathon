package com.dexify.app.myapplication;


import com.dexify.app.myapplication.network.params.RequestParams;

import java.util.HashMap;

public class ApiParams {
  private final int httpMethod;
  private final String url;
  private final HashMap<String, String> paramMap;
  private final HashMap<String, String> headerMap;

  public ApiParams(int method, String url) {
    this.httpMethod = method;
    this.url = url;
    this.paramMap = new HashMap<>();
    this.headerMap = new HashMap<>();
  }

  public void addParamPair(String key, String value) {
    paramMap.put(key, value);
  }

  public void addHeaderPair(String key, String value) {
    headerMap.put(key, value);
  }

  public RequestParams createRequestParams(Object tag) {
    return new RequestParams(httpMethod, url, headerMap, paramMap, tag);
  }

  /**
   * Returns the paramMap
   *
   * @return java.util.HashMap<> key-{@link String} ,value- {@link String} of paramMap
   */
  public HashMap<String, String> getParamMap() {
    return paramMap;
  }

  /**
   * Returns the headerMap
   *
   * @return java.util.HashMap<java.lang.String,java.lang.String> value of headerMap
   */
  public HashMap<String, String> getHeaderMap() {
    return headerMap;
  }

  /**
   * Returns the httpMethod
   *
   * @return int value of httpMethod
   */
  public int getHttpMethod() {
    return httpMethod;
  }

  /**
   * Returns the url
   *
   * @return java.lang.String value of url
   */
  public String getUrl() {
    return url;
  }

}
