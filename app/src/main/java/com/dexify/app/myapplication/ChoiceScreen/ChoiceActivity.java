package com.dexify.app.myapplication.ChoiceScreen;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.dexify.app.myapplication.RouteDetailScreen.RouteDetailActivity;
import com.dexify.app.myapplication.searchScreen.FormActivity;
import com.priya.app.myapplication.R;

public class ChoiceActivity extends AppCompatActivity implements IChoiceScreenView {

    ImageView txtExplore, txtRecommendation;
    IChoicePresenter iChoicePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice);
        iChoicePresenter = new ChoicePresenterImpl();
        initViews();
        iChoicePresenter.bindView(this);
    }

    private void initViews() {
        txtExplore = (ImageView) findViewById(R.id.txt_explore);
        txtRecommendation = (ImageView) findViewById(R.id.txt_recommend);
        // Create the RoundedBitmapDrawable.
        Bitmap explore = BitmapFactory.decodeResource(getResources(),R.drawable.superthumb);
        RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(getResources(), explore);
        roundDrawable.setCircular(true);
        txtExplore.setImageDrawable(roundDrawable);
        Bitmap recombitmap = BitmapFactory.decodeResource(getResources(),R.drawable.its_on_my_list);
        RoundedBitmapDrawable roundDrawable2 = RoundedBitmapDrawableFactory.create(getResources(), recombitmap);
        roundDrawable2.setCircular(true);
        txtRecommendation.setImageDrawable(roundDrawable2);
        txtExplore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iChoicePresenter.onExploreClick();
            }
        });
        txtRecommendation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iChoicePresenter.onRecommendationClick();
            }
        });
    }

    @Override
    public void wantToExplore() {
        startActivity(new Intent(this, FormActivity.class));
    }

    @Override
    public void seeRecommendation() {
        // TODO:
    }

    @Override
    public void showLoading(boolean show) {
        // not required here
    }
}
