package com.dexify.app.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by PRIYA on 08-Apr-17.
 */

public class RouteOrigin implements Parcelable {
    private String id;

    private String name;

    private String state;

    private String xid;

    private String lng;

    private String mongoId;

    private String lat;

    private String country;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getXid ()
    {
        return xid;
    }

    public void setXid (String xid)
    {
        this.xid = xid;
    }

    public String getLng ()
    {
        return lng;
    }

    public void setLng (String lng)
    {
        this.lng = lng;
    }

    public String getMongoId ()
    {
        return mongoId;
    }

    public void setMongoId (String mongoId)
    {
        this.mongoId = mongoId;
    }

    public String getLat ()
    {
        return lat;
    }

    public void setLat (String lat)
    {
        this.lat = lat;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", name = "+name+", state = "+state+", xid = "+xid+", lng = "+lng+", mongoId = "+mongoId+", lat = "+lat+", country = "+country+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.state);
        dest.writeString(this.xid);
        dest.writeString(this.lng);
        dest.writeString(this.mongoId);
        dest.writeString(this.lat);
        dest.writeString(this.country);
    }

    public RouteOrigin() {
    }

    protected RouteOrigin(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.state = in.readString();
        this.xid = in.readString();
        this.lng = in.readString();
        this.mongoId = in.readString();
        this.lat = in.readString();
        this.country = in.readString();
    }

    public static final Parcelable.Creator<RouteOrigin> CREATOR = new Parcelable.Creator<RouteOrigin>() {
        @Override
        public RouteOrigin createFromParcel(Parcel source) {
            return new RouteOrigin(source);
        }

        @Override
        public RouteOrigin[] newArray(int size) {
            return new RouteOrigin[size];
        }
    };
}
