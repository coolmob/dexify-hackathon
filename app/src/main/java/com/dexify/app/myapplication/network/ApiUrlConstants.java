package com.dexify.app.myapplication.network;

/**
 * Created by PRIYA on 08-Apr-17.
 */

public class ApiUrlConstants {
    public static final String SEARCH_FOR = "searchFor";
    public static final String NE_CATEGORIES = "neCategories";
    public static final String QUERY = "query";
    public static final String API_KEY = "apiKey";
    public static final String VAL_API_KEY = "ixicode!2$";
    public static final String ORIGIN_CITY_ID = "originCityId";
    public static final String DESTINATION_CITY_ID = "destinationCityId";
    public static final String ORIGIN = "origin";
    public static final String ORIGIN_CODE = "originCode";
    public static final String DESTINATION = "destination";
    public static final String DESTINATION_CODE = "destinationCode";

    public static final String ORIGIN_XID = "originXid";
    public static final String DESTINATION_XID = "destinationXid";
    public static final String MODE = "mode";
    public static final String ORIGIN_ID = "originId";
    public static final String DESTINATION_ID = "destinationId";
    public static final String FASTEST_CARRIER = "fastestCarrier";

    public static final String VAL_SEARCH_FOR = "tpAutoComplete";
    public static final String VAL_NE_CATEGORIES="City";

    public static final String TEXT="text";
    public static final String URL="url";
    public static final String CT="ct";
    public static final String ADDRESS="address";
    public static final String _ID="_id";
    public static final String CN="cn";
    public static final String EN="en";
    public static final String RT="rt";
    public static final String ST="st";
    public static final String CO="co";
    public static final String _OID="_oid";
    public static final String EID="eid";
    public static final String CID="cid";
    public static final String USE_NLP="useNLP";
    public static final String LAT="lat";
    public static final String LON="lon";
    public static final String XID="xid";
    public static final String PRICE = "price";
    public static final String TIME = "time";
    public static final String MODE_VIA_STRING = "modeViaString";
    public static final String TIME_UNIT = "timeUnit";
    public static final String CARRIER_NAME = "carrierName";
    public static final String DEP_TIME = "depTime";
    public static final String ARR_TIME = "arrTime";
    public static final String CARRIER = "carriers";
    public static final String STEPS = "steps";
    public static final String DATA = "data";
}
