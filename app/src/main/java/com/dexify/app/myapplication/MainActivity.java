package com.dexify.app.myapplication;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.dexify.app.myapplication.Utils.AMLog;
import com.dexify.app.myapplication.network.ApiCall;
import com.dexify.app.myapplication.network.ApiUrl;
import com.dexify.app.myapplication.network.ApiUrlConstants;
import com.dexify.app.myapplication.network.RequestComplete;
import com.dexify.app.myapplication.network.ResponseObject;
import com.priya.app.myapplication.R;

public class MainActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                hitsampleGetApi();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void hitsampleGetApi(){
        ApiParams apiParams = new ApiParams(Request.Method.GET, ApiUrl.AUTO_COMPLETE);
        apiParams.addParamPair(ApiUrlConstants.SEARCH_FOR, ApiUrlConstants.VAL_SEARCH_FOR);
        apiParams.addParamPair(ApiUrlConstants.NE_CATEGORIES, ApiUrlConstants.VAL_NE_CATEGORIES);
        apiParams.addParamPair(ApiUrlConstants.QUERY,"goa");
        ApiCall.stringGetRequestWithParam(apiParams.createRequestParams("autoComplete"), new RequestComplete() {
            @Override
            public void requestSuccess(ResponseObject object) {
                AMLog.d(object.toString());
            }

            @Override
            public void requestFailed(ResponseObject object) {
                AMLog.d(object.toString());
            }
        });
    }
}
